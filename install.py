#!/usr/bin/env python3

import shutil
from os.path import expanduser
import os

project = os.path.dirname(os.path.realpath(__file__))
home = expanduser("~")

bashrc_project = os.path.join(project, "bashrc")
bashrc_home = os.path.join(home, ".bashrc")

print("Copying bashrc...")
shutil.copyfile(bashrc_project, bashrc_home)
