* Dave's Dotfiles
  Because all of the cool kids version control their dotfiles, but
  only the coolest kids write their own install script in Scheme.

** Installing
   The dotfiles are installed by creating symlinks from the
   =dotfiles/= directory to the current user's home directory.

   To install, just run =./bootstrap && ./configure && make install=.
