(use-modules (guix packages)
	     (guix licenses)
	     (guix build-system trivial)
	     (gnu packages base)
	     (gnu packages autotools)
	     (gnu packages guile))

(package
 (name "dotfiles")
 (version "0")
 (source #f)
 (build-system trivial-build-system)
 (native-inputs
  `(("make" ,gnu-make)
    ("autoconf" ,autoconf)
    ("automake" ,automake)))
 (inputs
  `(("guile" ,guile-2.0)))
 (synopsis "Dave's dotfiles")
 (description "All of my glorious dotfiles.")
 (home-page "https://git.dthompson.us/dotfiles.git")
 (license gpl3+))
