export PATH=$HOME/src/bin:$HOME/.local/bin:$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games
export ENV="$HOME/.kshrc"
export CFLAGS="-W -Wall -Wextra -g"
export MAKEFLAGS="-j4"
export EDITOR="nvim"
