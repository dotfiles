require('impatient')

local map = vim.api.nvim_set_keymap
local opts = { noremap=true, silent=true }

-- General settings
vim.opt.mouse = 'a'
vim.opt.undofile = true
vim.opt.textwidth = 80
map('', 'Q', '<nop>', opts)
vim.g.mapleader = ' '

-- Plugins
local install_path = vim.fn.stdpath('data') .. '/site/pack/paqs/start/paq-nvim'
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
	vim.fn.system({'git', 'clone', '--depth=1',
	'https://github.com/savq/paq-nvim.git', install_path})
end

require('paq') {
	'dstein64/vim-startuptime';       -- Profile startup time
	'editorconfig/editorconfig-vim';  -- Follow editorconfig
	'folke/lsp-colors.nvim';          -- Add LSP colors to any theme
	'godlygeek/tabular';              -- Line things up
	'lewis6991/impatient.nvim';       -- Improve startup time
	'lifepillar/vim-mucomplete';      -- Simple completion
	'meain/hima-vim';                 -- Nice color scheme
	'nathom/filetype.nvim';           -- Faster filetype.vim replacement
	'neovim/nvim-lspconfig';          -- LSP configurations
	'norcalli/nvim-colorizer.lua';    -- Highlight css colors
	'ntpeters/vim-better-whitespace'; -- Highlight and strip whitespace
	'savq/paq-nvim';                  -- paq manages itself
	'tpope/vim-commentary';           -- Easily comment
	'tpope/vim-rsi';                  -- Readline bindings
	'tpope/vim-sensible';             -- Sensible defaults
	'tpope/vim-speeddating';          -- Modify dates with C-a, C-x
	'tpope/vim-surround';             -- Easily modify surrounding chars
}

-- Colorscheme
vim.opt.termguicolors = true
vim.g.colors_name = 'hima-plain'

-- Easier split movement
map('n', '<C-h>', '<C-w><C-h>', opts)
map('n', '<C-j>', '<C-w><C-j>', opts)
map('n', '<C-k>', '<C-w><C-k>', opts)
map('n', '<C-l>', '<C-w><C-l>', opts)

-- Clear search highlighting
map('n', '<C-c>', ':noh<CR>', opts)

-- Disable keyword completion
map('i', '<C-n>', '<Down>', opts)
map('i', '<C-p>', '<Up>', opts)

-- Completion
vim.opt.completeopt = vim.opt.completeopt + 'menuone'
vim.opt.completeopt = vim.opt.completeopt - 'preview'
vim.opt.shortmess = vim.opt.shortmess + 'c'

-- LSP
local lspconfig = require('lspconfig')

local on_attach = function(client, bufnr)
	-- Shorthand
	local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr,
		...) end
	local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr,
		...) end

	-- Use omnifunc for completion
	buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

	-- Keybindings
	buf_set_keymap('n', 'gD',
		'<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
	buf_set_keymap('n', 'gd',
		'<cmd>lua vim.lsp.buf.definition()<CR>', opts)
	buf_set_keymap('n', 'gr',
		'<cmd>lua vim.lsp.buf.references()<CR>', opts)
	buf_set_keymap('n', 'K',
		'<cmd>lua vim.lsp.buf.hover()<CR>', opts)
	buf_set_keymap('n', '<leader>rn',
		'<cmd>lua vim.lsp.buf.rename()<CR>', opts)
	buf_set_keymap('n', '[d',
		'<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
	buf_set_keymap('n', ']d',
		'<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
	buf_set_keymap('n', '<leader>e',
		'<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
end

-- Add servers
local servers = {
	'clangd',
	'racket_langserver',
}

for _, server in ipairs(servers) do
	lspconfig[server].setup {on_attach = on_attach}
end

vim.opt.signcolumn = 'no'

-- Jump to files with gf
vim.opt.hidden = true
vim.opt.path = vim.opt.path + '**'

-- Disable intro message
vim.opt.shortmess = vim.opt.shortmess + 'I'

-- Highlight on yank
vim.cmd([[
augroup highlight_yank
    autocmd!
    au TextYankPost * silent! lua vim.highlight.on_yank{timeout=200}
augroup END
]])

-- Do not source the default filetype.vim (uses nathom/filetype.nvim instead)
vim.g.did_load_filetypes = 1
