(use-modules (gnu packages autotools)
             (gnu packages guile)
             (guix packages))

(packages->manifest
 (list autoconf
       automake
       gnu-make
       guile-3.0))
