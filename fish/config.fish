set -x GOBIN "$HOME/.local/bin"
set -x EDITOR "nvim"
set -x PASSWORD_STORE_GPG_OPTS "--no-throw-keyids"
set -x DELTA_PAGER "less -R"
set -x CARGO_INSTALL_ROOT "$HOME/.local"
if status is-interactive
    gpg-connect-agent updatestartuptty /bye >/dev/null 2>&1
end
