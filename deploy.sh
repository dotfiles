#!/bin/sh

stow aptitude
stow awesome
stow bash
stow bspwm
stow conky
stow emacs
stow evolution
stow nextcloud
stow rofi
stow sxhkd
stow tmux
stow vim
