# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history.
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# set big history
export HISTSIZE=100000
export HISTFILESIZE=$HISTSIZE

# append to history after each command finishes
export PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

git_color() {
    branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
    status=$(git status --porcelain -b 2>/dev/null)
    if [[ "$status" == "## ${branch}...origin/${branch}" ]]; then
        echo -e "\033[0;32m" # green
    else
        echo -e "\033[0;31m" # red
    fi
}

git_branch() {
    a=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
    if [ -n "$a" ]; then
        echo " [$a]"
    else
        echo ""
    fi
}

# set fancy prompt
if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # color supported
    #PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[32m\]╭─ \u@\h \w ─╼\n╰╼ \$\[\033[00m\] "
    #PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[32m\]\u@\h \w\n\$\[\033[00m\] "
    PS1="\[\e]0;\u@\h: \w\a\]\[\e]0;\u@\h: \w\a\]\[\033[32m\]\u@\h \w\[\$(git_color)\]\$(git_branch)\n\[\033[0;32m\]$\[\033[00m\] "
else
    # no color
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi

# set xterm title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of various utilities
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# load ls_colors configuration if it exists (Arch)
if [ -f /usr/share/LS_COLORS/dircolors.sh ]; then
    . /usr/share/LS_COLORS/dircolors.sh
fi

# ever seen a monochrome xterm?  I sure haven't
if [ "$TERM" = "xterm" ]; then
    export TERM=xterm-256color
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# aliases
alias open='xdg-open'

# environment
export LANG="en_CA.UTF-8"
export LC_MEASUREMENT="en_CA.UTF-8"
export LC_MONETARY="en_CA.UTF-8"
export LC_NUMERIC="en_CA.UTF-8"
export LC_PAPER="en_CA.UTF-8"
export LC_TIME="en_DK.UTF-8"
export WINDOW_MANAGER="/usr/bin/bspwm"
export GTK_IM_MODULE="xim"
export EDITOR="emacsclient"
export PAGER="less"
export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:/usr/lib/pkgconfig"
export VAGRANT_DEFAULT_PROVIDER=libvirt

# colorize man
man() {
	env \
		LESS_TERMCAP_mb=$(printf "\e[31m") \
		LESS_TERMCAP_md=$(printf "\e[1;32m") \
		LESS_TERMCAP_me=$(printf "\e[0m") \
		LESS_TERMCAP_se=$(printf "\e[0m") \
		LESS_TERMCAP_so=$(printf "\e[36m") \
		LESS_TERMCAP_ue=$(printf "\e[0m") \
		LESS_TERMCAP_us=$(printf "\e[33m") \
			man "$@"
}

if [ -d "$HOME/.cargo/bin" ] ; then
	PATH="$HOME/.cargo/bin:$PATH"
fi

if [ -d "$HOME/.gem/ruby/2.6.0/bin" ] ; then
	PATH="$HOME/.gem/ruby/2.6.0/bin:$PATH"
fi

if [ -d "$HOME/.local/lib/node_modules/bin" ] ; then
	PATH="$HOME/.local/lib/node_modules/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d "/usr/lib/ccache/bin" ] ; then # Arch
	PATH="/usr/lib/ccache/bin:$PATH"
elif [ -d "/usr/lib/ccache" ] ; then # Debian
	PATH="/usr/lib/ccache:$PATH"
fi
