set wildmode=longest,list
set wildmenu

set ts=4
set shiftwidth=4
set bs=2
set showmatch
set tags=./tags

set hlsearch
set incsearch

set laststatus=2
set ruler

set noexpandtab

set showbreak=>>>

set guioptions-=T

:syntax on
