let b:match_words = &matchpairs
                    \ . ',%if:%else:%endif'
                    \ . ',%macro\>:%endmacro\>'
                    \ . ',%rep\>:%endrep\>'
