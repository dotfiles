syn keyword x86incInstruction MOVA MOVH
syn keyword x86incInitCpuflags INIT_MMX INIT_XMM INIT_YMM INIT_ZMM
syn keyword x86incCpuflags mmx mmx2 mmxext 3dnow 3dnowext sse sse2 sse2slow sse3 ssse3 sse4 sse42 avx avx2 xop fma4 fma3
syn keyword x86incDefs cextern cglobal

hi link x86incInitCpuflags Special
hi link x86incInstruction Statement
hi link x86incCpuflags Define
hi link x86incDefs Define
