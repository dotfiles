# -*- shell-script -*-

# Honor per-interactive-shell startup file
if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi

if [ -f $HOME/.guix-profile/etc/profile ]
then
    GUIX_PROFILE=$HOME/.guix-profile
    source $GUIX_PROFILE/etc/profile
fi

export PATH="$HOME/.config/guix/current/bin${PATH:+:}$PATH"
export EDITOR=emacsclient
