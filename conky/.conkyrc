conky.config = {
   -- conkyrc

   alignment = 'top_left',
   border_inner_margin = 1,
   border_outer_margin = 0,
   border_width = 0,

   -- color of active workspace
   color1 = '#4E9A40',

   -- inactive workspaces and divider color
   color2 = '#808080',

   -- workspaces which have windows but aren't focused
   color3 = '#808020',

   -- urgency hinting color
   color4 = '#f9d72d',

   -- color of temps and percentages
   color5 = '#8F8F00',

   -- Graph outlines
   color6 = '#3F3B3B',

   default_color = '#3F3B3B',
   double_buffer = true,
   draw_borders = true,
   draw_graph_borders = true,
   draw_shades = false,
   gap_x = 13,
   gap_y = 6,

   -- adjust to the width of your screen
   maximum_width = 2854,
   minimum_width = 2854,
   minimum_height = 0,

   own_window = true,
   own_window_argb_visual = false,

   -- bar background color
   own_window_colour = '#000',

   own_window_type = 'panel',
   own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
   pad_percents = 2,
   update_interval = 1,
   use_spacer = 'right',
   use_xft = true,
   --xftalpha = .2,

   -- choose font and size
   font = 'Monospace:size=16',

};

conky.text = [[
${execp bspc control --get-status | cut -d ":" -f 2 | xargs ~/bin/workspaces.sh}\
\
${color2}${alignc}CPU $color6${voffset 3}${cpugraph 20,34 859900 DC322F -t}${voffset -3}${offset 7}$color2\
${execp echo $(cat /sys/bus/platform/devices/coretemp.0/hwmon/hwmon*/temp1_input) / 1000 | bc}°\
\
      Mem $color6${voffset 2}${memgraph 22,34 859900 DC322F -t}${voffset -2}${offset 7}$color2${memperc}%\
\
      Disk ↓${offset 2}${voffset 2}$color6${diskiograph_read /dev/sda 22,34 859900 DC322F -t}${offset 7}$color2${voffset -2}${diskio_read /dev/sda}\
 ↑${offset 2}${voffset 2}$color6${diskiograph_write /dev/sda 22,34 859900 DC322F -t}${offset 7}$color2${voffset -2}${diskio_write /dev/sda}\
     Net ↓${offset 2}${voffset 2}$color6${downspeedgraph wlan0 22,34 859900 DC322F -t}${voffset -2}${offset 7}$color2${downspeed wlan0}\
 ↑${offset 2}${voffset 2}$color6${upspeedgraph wlan0 22,34 859900 DC322F -t}${voffset -2}${offset 8}$color2${upspeed wlan0}\
\
  ${execp if [ "$(cat /sys/bus/acpi/drivers/ac/ACPI0003\:00/power_supply/ADP1/online)" -eq "1" ]; then echo '+'; else echo '-'; fi}${battery_percent}%\
\
$alignr${time %a %Y-%m-%d %H:%M}\
]];

--#${execp udisks --show-info /dev/sda | grep temperature | sed 's/.* \([0-9]*\)C.*/\1/'}°\
