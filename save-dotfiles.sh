#!/bin/sh

repo_path="${HOME}/Workspace/dotfiles"

cp ~/.Xresources ${repo_path}/Xresources
cp ~/.vimrc ${repo_path}/vimrc
cp -r ~/.irssi ${repo_path}/irssi
cp ~/.muttrc ${repo_path}/muttrc
cp ~/.mkshrc ${repo_path}/mkshrc
cp ~/.gitconfig ${repo_path}/gitconfig
cp -r ~/.mutt ${repo_path}/mutt
cp -r ~/.vim ${repo_path}/vim
cp -r ~/.config/herbstluftwm/ ${repo_path}/herbstluftwm
cp -r ~/.config/termcolors/ ${repo_path}/termcolors
cp -r ~/scripts/ ${repo_path}/scripts
cp -r ~/.mpd/ ${repo_path}/mpd
cp -r ~/.ncmpcpp/ ${repo_path}/ncmpcpp

echo "Now check your passwords :)"
