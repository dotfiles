local cmp_nvim_lsp = require("cmp_nvim_lsp")
local cmp = require("cmp")
local fidget = require("fidget")
local lspconfig = require("lspconfig")
local lualine = require("lualine")
local rust_tools = require("rust-tools")
local snippy = require("snippy")
local telescope = require("telescope")
local telescope_builtin = require("telescope.builtin")
local tokyonight = require("tokyonight")
local nvim_treesitter_configs = require("nvim-treesitter.configs")

vim.cmd([[
colorscheme sonokai
hi clear Pmenu
]])

vim.opt.mouse = "a"
vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.number = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.showmode = false
vim.opt.scrolloff = 5
vim.opt.spelllang = "en_us"
vim.opt.swapfile = false
vim.opt.wrap = true
vim.opt.showbreak = ">>> "
vim.opt.textwidth = 100
vim.opt.colorcolumn = "+1"
vim.opt.termguicolors = true
vim.opt.background = "dark"
vim.opt.breakindent = true
vim.opt.breakindentopt = "list:-1"

vim.g.zig_fmt_autosave = 0

vim.keymap.set("i", "jj", "<Esc>", {noremap = true, silent = true})
vim.keymap.set('n', '<space>e', telescope_builtin.diagnostics)
vim.keymap.set('n', '<space>ff', telescope_builtin.find_files)
vim.keymap.set('n', '<space>fg', telescope_builtin.live_grep)
vim.keymap.set('n', '<space>fb', telescope_builtin.buffers)
vim.keymap.set('n', '<space>fh', telescope_builtin.help_tags)
vim.keymap.set('n', '<space>fs', telescope_builtin.lsp_document_symbols)
vim.keymap.set('n', '<space>f\'', telescope_builtin.resume)
vim.keymap.set('n', 'z=', telescope_builtin.spell_suggest)
vim.keymap.set({"i", "n"}, "<F3>", function()
  vim.opt.spell = not vim.opt.spell:get()
end, {noremap = true, silent = true})

local lsp_formatting_group = vim.api.nvim_create_augroup("LspFormatting", {})
local common_setup = {
  capabilities = cmp_nvim_lsp.default_capabilities(),
  on_attach = function(client, bufnr)
    local bufopts = { noremap = true, silent = true, buffer = bufnr }
    vim.keymap.set('n', 'gD', telescope_builtin.lsp_type_definitions, bufopts)
    vim.keymap.set('n', 'gd', telescope_builtin.lsp_definitions, bufopts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
    vim.keymap.set('n', 'gi', telescope_builtin.lsp_implementations, bufopts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
    vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, bufopts)
    vim.keymap.set('n', 'gr', telescope_builtin.lsp_references, bufopts)
    vim.keymap.set('n', '<space>f', function()
      vim.lsp.buf.format()
    end, bufopts)
    vim.keymap.set('n', '<space>d', function()
      vim.diagnostic.open_float()
    end, bufopts)
    vim.api.nvim_clear_autocmds({
      group = lsp_formatting_group,
      buffer = bufnr,
    })
    vim.api.nvim_create_autocmd("BufWritePre", {
      group = lsp_formatting_group,
      buffer = bufnr,
      callback = function()
        vim.lsp.buf.format({async = false})
      end
    })
  end,
}

lspconfig.zls.setup(common_setup)
lspconfig.clangd.setup({
  cmd = { "/usr/lib/llvm18/bin/clangd" },
  capabilities = common_setup.capabilities,
  on_attach = common_setup.on_attach,
})
lspconfig.nim_langserver.setup(common_setup)
lspconfig.csharp_ls.setup(common_setup)
lspconfig.eslint.setup(common_setup)
lspconfig.html.setup(common_setup)
lspconfig.jsonls.setup(common_setup)
lspconfig.cssls.setup(common_setup)
lspconfig.svelte.setup(common_setup)

lspconfig.gopls.setup({
  settings = {
    gopls = {
      gofumpt = true,
    },
  },
  capabilities = common_setup.capabilities,
  on_attach = common_setup.on_attach,
})

rust_tools.setup({
  server = {
    capabilities = common_setup.capabilities,
    on_attach = function(client, bufnr)
      common_setup.on_attach(client, bufnr)
      local bufopts = { noremap = true, silent = true, buffer = bufnr }
      vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, bufopts)
      vim.keymap.set("n", "<space>a", rt.code_action_group.code_action_group, bufopts)
    end,
  },
})

fidget.setup({})

cmp.setup({
  snippet = {
    expand = function(args)
      snippy.expand_snippet(args.body)
    end,
  },
  sources = {
    { name = "nvim_lsp" },
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-u>'] = cmp.mapping.scroll_docs(-4), -- Up
    ['<C-d>'] = cmp.mapping.scroll_docs(4), -- Down
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    }),
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
})

lualine.setup()

nvim_treesitter_configs.setup({
  ensure_installed = {
    "zig",
    "wgsl",
    "lua",
    "go",
    "nim",
    "c_sharp",
    "javascript",
    "html",
    "css",
    "svelte",
    "markdown",
  },
  highlight = { enable = true },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<CR>',
      scope_incremental = '<CR>',
      node_incremental = '<TAB>',
      node_decremental = '<S-TAB>',
    },
  },
})

telescope.setup({
  defaults = {
    mappings = {
      i = {
        ["<esc>"] = require('telescope.actions').close,
      },
    }
  },
})
