[ $(hostname) = "alcatraz.armaanb.net" ] && {
	date
	apm | head -n 1
}

set -o vi

alias pman='man -M /usr/local/share/doc/posix/man'
alias papropos='apropos -M /usr/local/share/doc/posix/man'
alias pwhatis='whatis -M /usr/local/share/doc/posix/man'

alias rsync='openrsync -rv'

ufile() {
    [ "$(file -bi "$1")" = "text/plain" ] && ext="txt"
    ext=$(printf "%s" "$1" | awk -F . '{print $NF}')
    fname="$(basename ${2:-tmp$RANDOM.$ext})"
    rsync "$1" "l.armaanb.net:/var/www/htdocs/l.armaanb.net/$fname"
    printf "https://l.armaanb.net/%s\n" "$fname" | xsel -sel c
}

export GPG_TTY="$(tty)"
export CDPATH=":~"

alias ls="LC_COLLATE=C ls -lhF"
alias df="df -h"
alias du="du -h"
alias cp="cp -riv"
alias rm='rm -iv'
alias mv='mv -iv'
alias grep='grep -in'
alias mkdir='mkdir -p'
alias nl='nl -b all'
alias rclone='rclone -P'

alias ytmusic="youtube-dl --add-metadata --extract-audio \
	--audio-format vorbis --prefer-ffmpeg --restrict-filenames \
	-o '%(title)s.%(ext)s'"
alias mosh="env LC_CTYPE=en_US.UTF-8 mosh"
alias serve="python3 -m http.server"
alias wttr='curl wttr.in?format="%l+%T\n%C+%t\nSunrise:+%S,+Sunset+%s\n"'
alias nv="nvim"
alias dotgit="git --git-dir=~/.local/share/dotfiles --work-tree=~"

pass() {
	printf "Master password: "
	stty -echo
	read masterpass
	stty echo
	sqlcipher .passwords.db -column -cmd "PRAGMA key = \"$masterpass\"" "$1"
}

plike() {
	pass "SELECT * FROM p WHERE name LIKE \"%$1%\";"
}

pget() {
	pass "SELECT * FROM p WHERE name = \"$1\";"
}

pinsert() {
	printf "New password: "
	stty -echo
	read newpass
	stty echo
	pass "INSERT INTO p VALUES (\"$1\", \"$2\", \"$newpass\");"
}

pgen() {
	tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' < /dev/urandom \
		| fold -w 20 \
		| head -n 1
}


dosa() {
	echo "Dosa! Yum yum yum"
	doas $@
}

watch() {
	inp=$1
	shift
	fwa $inp | while read; do $@; done
}

stty dsusp undef
