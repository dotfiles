#!/bin/sh

rm ~/.aptitude
rm ~/.config/awesome
rm ~/.bashrc
rm ~/.config/bspwm
rm ~/.conkyrc
rm ~/.emacs.d
rm ~/.config/evolution
rm ~/.config/nextcloud
rm ~/.config/rofi
rm ~/.config/sxhkd
rm ~/.tmux.conf
rm ~/.vimrc
