filetype plugin indent on
syntax on

set runtimepath+=/usr/share/vim/vimfiles/

call pathogen#infect()

" Color
"let g:hybrid_use_Xresources = 1

if has("gui_running")
    set guioptions=c
    colo hybrid
    set bg=dark
    "set guifont=Envy\ Code\ R\ 9
    set guifont=Fira\ Mono\ Regular\ 9
else
    set bg=light
endif

" Settings
set noerrorbells
set novisualbell

set showcmd
set autoread

set nobackup
set nowb
set noswapfile
set wrap
set scrolloff=2

if expand("%:p") =~ '\.*blender\.*'
    set tabstop=4
    set shiftwidth=4
    set smarttab
    set autoindent
    set smartindent
    set ci
    set cin
    set ofu=syntaxcomplete#Complete
    set showmatch
    set incsearch
else
    set expandtab
    set smarttab
    set sw=4
    set ts=4
    set sts=4
    set ai
    set si
endif 

let NERDTreeIgnore=['.pyc$']

" Filetyp dependent

au BufRead /tmp/mutt-* set tw=80
au BufRead,BufNewFile *.md set tw=80 ft=markdown
au Bufread,BufNewFile *.go set ft=go

au Bufread,BufNewFile *.oz set ft=oz

au Bufread,BufNewFile *.rb set sw=2 ts=2 sts=2

au BufRead,BufNewFile *.html call HtmlDjangoSettings()
au BufRead,BufNewFile ~/.notes call Notes()

function! Notes()
    set ft=markdown
    set foldmethod=marker
    colo hybrid
    set tw=80
endfunction

function! HtmlDjangoSettings()
    if (&ft=='htmldjango')
        inoremap <Leader>t {%  %}<Esc>hhi
        inoremap <Leader>s {{  }}<Esc>hhi
    endif
endfunction

" Some functions
set colorcolumn=0
function! Toggle_colorcolumn()
    if &colorcolumn == 0
        set colorcolumn=80
    else
        set colorcolumn=0
    endif
endfunction


""""" Mappings """"""
" Setting nop to the arrows keys
nmap <F1> <Esc>
map <up> <nop>
map <down> <nop>
map <right> <nop>
map <left> <nop>
imap <up> <nop>
imap <down> <nop>
imap <right> <nop>
imap <left> <nop>

nnoremap <F2> :NERDTreeToggle<cr>
nmap <F3> :TagbarToggle<cr>

map <c-h> <c-w>h
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l

" Yank behaves really good
map Y y$

" Nice for wrapped lines
nnoremap j gj
nnoremap k gk
inoremap jk <Esc>

nnoremap <Leader>p :call Toggle_colorcolumn() <cr>
" Diff
nnoremap <F4> :diffg 3 <cr>
