#!/bin/bash

# This prints a character '♫' and the percentage of the actual volume.
# ♫ = E299AB
WITH_ICONS=false

get_percentage() {
    full_db=65.25
    # -65.25 <= $db <= 0
    db=$1
    bc <<CALC scale=2
(-$db)/$full_db
CALC
}

get_icon() {
    if [ "$volStatus" == "on" ]
    then
        echo "\fr♫"
    else
        echo "\f1♫"
    fi
}

panel_volume()
{

    volStatus=$(amixer get Master | tail -n 1 | cut -d '[' -f 4 | sed 's/].*//g')
    volLevel=$(amixer get Master | tail -n 1 | cut -d '[' -f 2 | sed 's/\%].*//g')
    if $WITH_ICONS
    then
        headphone=$(amixer -c 0 contents | grep 'Headphone Jack' -A 2 | tail -n 1 | sed s/.*=//g)
    else
        headphone=""
    fi

    if $WITH_ICONS
    then
        if [ "$volStatus" == "on" ] && [ "$headphone" == "off" ];
        then
            icon="\fr ⮞"
        elif [ "$volStatus" == "on" ] && [ "$headphone" == "on" ];
        then
            icon="\fr ⮜"
        elif [ "$volStatus" == "off" ] && [ "$headphone" == "on" ];
        then
            icon="\fr ⮜"
        elif [ "$volStatus" == "off" ] && [ "$headphone" == "off" ];
        then
            icon="\fr ⮝"
        else
            icon="\f1 ⮝"
        fi
    else
        icon=$(get_icon)
    fi

    case $volLevel in
        100)
            if [ "$volStatus" == "on" ]
            then 
                echo "$icon" "\fr——————————\fr"
            else 
                echo "$icon" "\f8——————————\fr"
            fi
            ;;
        [0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr—\f8—————————\fr"
            else
                echo "$icon" "\f1—\f8—————————\fr"
            fi
            ;;
        1[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr——\f8————————\fr"
            else
                echo "$icon" "\f1——\f8————————\fr"
            fi
            ;;
        2[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr———\f8———————\fr"
            else
                echo "$icon" "\f1———\f8———————\fr"
            fi
            ;;
        3[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr————\f8——————\fr"
            else
                echo "$icon" "\f1————\f8——————\fr"
            fi
            ;;
        4[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr—————\f8—————\fr"
            else
                echo "$icon" "\f1—————\f8—————\fr"
            fi
            ;;
        5[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr——————\f8————\fr"
            else
                echo "$icon" "\f1——————\f8————\fr"
            fi
            ;;
        6[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr———————\f8———\fr"
            else
                echo "$icon" "\f1———————\f8———\fr"
            fi
            ;;
        7[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr————————\f8——\fr"
            else
                echo "$icon" "\f1————————\f8——\fr"
            fi
            ;;
        8[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr——————————\f8—\fr"
            else
                echo "$icon" "\f1——————————\f8—\fr"
            fi
            ;;
        9[0-9])
            if [ "$volStatus" == "on" ]
            then
                echo "$icon" "\fr——————————\fr"
            else
                echo "$icon" "\f1——————————\fr"
            fi
            ;;
    esac
}

panel_volume
