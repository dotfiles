#!/bin/bash

# ./mvrs WINID X Y Width Height

floating=$(herbstclient get_attr tags.focus.floating)

if [[ x"$floating" == xfalse ]]
then
    resizestep=0.02
    herbstclient resize $1 $2
else
    resizestep=15
    winid="$(herbstclient get_attr clients.focus.winid)"

    width=$(xwininfo -id "${winid}" | grep Width | awk '{print $2}')
    height=$(xwininfo -id "${winid}" | grep Height | awk '{print $2}')

    case $1 in
        left)
            echo match left
            width=$(($width - ${resizestep}))
            ;;
        right)
            echo match right
            width=$(($width + ${resizestep}))
            ;;
        up)
            echo match up
            height=$(($height - ${resizestep}))
            ;;
        down)
            # Dunno, this is weird
            resizestep=20
            height=$(($height + ${resizestep}))
            ;;
    esac


    echo resizing to ${winid} $width $height
    ~/.config/herbstluftwm/bin/mvrs ${winid} $width $height
fi
