#!/bin/bash

source ${HOME}/.config/herbstluftwm/fn.sh

dzen_bg="$(get_xresources foreground)"
dzen_fg="$(get_xresources background)"

calendary() {
    (
        cal
    ) | dzen2 -p -bg "${dzen_bg}" -fg "${dzen_fg}" -x "1195" -y "27" -w "165" -h "20" -l "5" -sa "c" -ta "c" \
        -title-name "popup_sysinfo" -e 'onstart=uncollapse;button1=exit;button3=exit'
}

position(){
    pos=$(mpc | awk 'NR==2' | awk '{print $4}' | sed 's/(//' | sed 's/%)//')
    bar=$(echo $pos | gdbar -w $1 -h 3.5 -fg "$(get_xresources color12)" -bg "$(get_xresources color8)")
    echo -n "$bar"
    return
}

music() {
    set -e

    popup_width=220
    padding=10

    while :
    do
    echo "^p(10)$(mpc current -f %artist%)
^p($padding)$(mpc current -f %title%) 
^p($padding)$(mpc current -f %album%)

^p($padding)^ca(1,mpc prev)Prev^ca()   ^ca(1,mpc toggle)Play/Pause^ca()   ^ca(1,mpc stop)Stop^ca()   ^ca(1,mpc next)Next^ca()
^p($padding)$(position $(($popup_width - $padding)) )" 2> /dev/null
    sleep 1
    done | dzen2 -p -bg "${dzen_bg}" -fg "${dzen_fg}" -y 27 -x 1087 -l 5 -u -w $(($popup_width + $padding)) -ta l -title-name "popup_mpd" -e 'onstart=uncollapse;button3=exit'
}

case $1 in
    cal)
        calendary
        ;;
    music)
        music
        ;;
    *)
        ;;
esac
