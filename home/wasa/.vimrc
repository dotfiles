filetype plugin on
syntax on
colorscheme default
set background=light
au VimEnter * if &diff | execute 'windo set wrap' | endif
