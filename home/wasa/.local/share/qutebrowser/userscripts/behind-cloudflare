#!/usr/bin/env ruby
require 'http'
require 'resolv'
require 'uri'

QUTE_FIFO = ENV['QUTE_FIFO']
$resolver = Resolv::DNS.new
$resolver.timeouts = 1

def snd(path, msg)
  File.open(path, 'w') { |f| f.puts msg }
end

def api_request(url)
  res = HTTP.headers(accept: 'application/json').get(url)
  raise("HTTP error #{res.status.to_i}") unless res.status.ok?
  res.parse
end

def ip_addresses(url)
  $resolver.getaddresses(URI(url).host).map(&:to_s)
end

def cloudflare?(url)
  ip_addresses(url).find do |ip|
    meta = api_request("https://api.iptoasn.com/v1/as/ip/#{ip}")
    meta['as_description'][/CLOUDFLARE/]
  end
end

url = ENV['QUTE_URL'] || die('no url')
snd(QUTE_FIFO, ":message-info 'Cloudflare: #{cloudflare?(url) ? 'Yes' : 'No'}'")
