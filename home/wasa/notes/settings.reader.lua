-- we can read Lua syntax here!
return {
    ["filemanagermenu_tab_index"] = 5,
    ["quickstart_shown_version"] = 201512735,
    ["show_hidden"] = true,
    ["dicts_disabled"] = {},
    ["frontlight_intensity"] = 50,
    ["lastfile"] = "/mnt/sd/Fiction/Николай Васильевич Гоголь - Петербургские повести/Николай Васильевич Гоголь - Невский проспект.fb2",
    ["start_with"] = "last",
    ["is_frontlight_on"] = false,
    ["poweroff_screensaver_type"] = "random_image",
    ["screensaver_type"] = "random_image",
    ["poweroff_screensaver_dir"] = "/mnt/onboard/.screensaver-poweroff",
    ["home_dir"] = "/mnt/sd",
    ["lastdir"] = "/mnt/sd",
    ["coverbrowser_initial_default_setup_done"] = true,
    ["device_id"] = "D112F16179FB4132AFFC65FF7D965036",
    ["screensaver_dir"] = "/mnt/onboard/.screensaver-suspend",
    ["statistics"] = {
        ["is_enabled"] = true,
        ["min_sec"] = 5,
        ["convert_to_db"] = true,
        ["max_sec"] = 120
    }
}
