autoload -U tetris
autoload -U compinit promptinit colors
autoload -Uz vcs_info
compinit
promptinit
colors
setopt promptsubst \
  incappendhistory histverify sharehistory \
  histsavenodups histignorespace histreduceblanks histignorealldups
zstyle ':vcs_info:*' enable hg git bzr svn
precmd() {
  if [[ ! -f HEAD ]]; then # avoid bare git repo error
    vcs_info
  fi
}
setopt COMPLETE_ALIASES
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' rehash true

ZLE_REMOVE_SUFFIX_CHARS=$' \t\n;'

bindkey "^[[3~" delete-char
bindkey "^[3;5~" delete-char

HISTFILE=~/.config/zsh/.zhistory
HISTSIZE=10000
SAVEHIST=$HISTSIZE
READNULLCMD=$PAGER

if [[ $TERM == dumb ]]; then
  unset zle_bracketed_paste
fi

alias ls='ls --color=auto'
alias grep='grep --color=auto'

compdef sc=systemctl
compdef jc=journalctl
compdef pc=pacman
alias sc=systemctl
alias jc=journalctl
alias pc=pacman

alias pb=pastebin
alias newsboat="newsboat -q"
alias aurget="env -C ~/tmp aur fetch -r"
alias chicken-clean="bash -c 'rm -f *.sh *.so *.link *.o *.import.scm' && touch *.egg"

alias album-lengths="find ~/music/todo -mindepth 1 -type d -exec album-length '{}' \;"

whodis() {
  echo "$1" | nc "$(echo "$1" | nc whois.iana.org 43 | awk '/^whois:/ {print $2}')" 43
}

enable_networks() {
  for i in $(wpa_cli list_networks | tail -n+3 | cut -f1); do
    wpa_cli enable_network $i >/dev/null
  done
}

eval $(keychain --eval --quiet id_ed25519)

fortune
stty -ixon
xrdb ~/.Xresources
PROMPT="%(?.%F{green}; ;.%F{red};_;)%f ${RANGER_LEVEL:+"($RANGER_LEVEL) "}"
RPROMPT='${vcs_info_msg_0_}'
PS2='... '
