{:user {:plugins [[cider/cider-nrepl "0.21.1"]]
        :injections [(require '[clojure.pprint :refer [pprint]])
                     (defn dbg
                       ([thing]
                        (dbg "XXX" thing))
                       ([prefix thing]
                        (println prefix)
                        (print (with-out-str (pprint thing)))
                        thing))
                     (intern 'clojure.core 'dbg dbg)]}}
