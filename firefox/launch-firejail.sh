#!/bin/bash
#
# Firefox launcher, to be run inside a firejail sandbox

FIREFOX=/usr/bin/firefox-esr

if [ -d "$DOWNLOADS" -a ! -a ~/downloads ]; then
    ln -s "$DOWNLOADS" ~/downloads
fi

[ -a ~/.tridactylrc ] || ln -s ~/.mozilla/tridactylrc ~/.tridactylrc

# whitelisting ~/.config/zathura seems broken, not sure why
mkdir -p "$HOME/.config/zathura" && ln -s "$HOME/.config/dotfiles/dotfiles/zathurarc" "$HOME/.config/zathura/zathurarc"

CMD=$FIREFOX

APULSE=$(which apulse)
[ -n "$APULSE" ] && CMD="$APULSE $CMD"

$CMD "$@"
