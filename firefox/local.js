// dotfiles-managed system-wide preferences
// ideally should have been user-local, but there is no place for that

// firefox auto-config, does nothing currently
//pref('general.config.filename', 'firefox.cfg');
pref('general.config.obscure_value', 0);

// disable quitting with C-q
pref("browser.quitShortcut.disabled", true);
// closing last tab does NOT close the program
pref("browser.tabs.closeWindowWithLastTab", false);

// warn on closing multiple tabs
pref('browser.tabs.warnOnClose', true);

// load userChrome and userContent CSS
pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

// kill pocket
pref("extensions.pocket.enabled", false);

// always show scrollbar
pref("layout.testing.overlay-scrollbars.always-visible", true);

// sane scrollbar appearance
pref("widget.non-native-theme.scrollbar.style", 4);
pref("widget.non-native-theme.scrollbar.size.override", 10);

pref("general.smoothScroll.mouseWheel", false);
pref("general.smoothScroll.lines", false);
pref("general.smoothScroll.scrollbars", false);
pref("general.smoothScroll.other", false);
pref("general.smoothScroll.pages", false);
pref("general.smoothScroll.pixels", false);
pref("general.smoothScroll", false);

// kill automatic translation popups
pref("browser.translations.automaticallyPopup", false);

// dark mode
// XXX: don't seem to actually work
pref("browser.in-content.dark-mode", true);
pref("ui.systemUsesDarkTheme", true);

pref("media.autoplay.default", 0);
