#!/bin/bash
# script for setting up and launching firefox under firejail

NAME=firefox
LAUNCHER="/usr/bin/firejail"
# presence of this file in a subdir of ~/.mozilla/firefox identifies it
# as a profile subdir
PROFILE_CANARY=prefs.js

if [ ! -x "$LAUNCHER" ]; then
    echo "$LAUNCHER not present, launching directly" >&2
    /usr/bin/firefox-esr "$@"
fi

export DOWNLOADS=$(realpath -e ~/downloads/firefox)

# if profile is requested, add it to the jail name
while getopts ":P:" OPT; do
     case "$OPT" in
        P)
            PROFILE=$OPTARG
            NAME=$NAME-$PROFILE
            break
            ;;
    esac
done

# option host-local configuration file that can override this
# script's variables
LOCAL_CONFIG=${HOME}/.mozilla/launch_local.sh
[ -f "$LOCAL_CONFIG" ] && . "$LOCAL_CONFIG"

# install userChrome.css to all profiles that do not have it
for f in ~/.mozilla/firefox/*; do
    [ -f "$f/$PROFILE_CANARY" ] || continue

    DSTFILE="$f/chrome/userChrome.css"

    mkdir -p "$(dirname $DSTFILE)"
    [ -f "$DSTFILE" ] || ln -s ~/.mozilla/userChrome.css "$DSTFILE"
done

$LAUNCHER --profile=firefox-esr --name="$NAME" ~/.mozilla/firefox-run-firejail "$@"
