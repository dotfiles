(defun ceg/show-urls (what)
  "Run show-urls and grep for WHAT."
  (interactive "sUrl: ")
  (shell-command (concat "python " ceg/manage-bin " show_urls --no-color | grep '" what "'")
		 (concat "*URL<" what ">")))

(defun my/show-urls (what)
  "Run show_urls and grep for WHAT."
  (interactive "sUrl: ")
  (let ((command (concat "python " ceg/manage-bin " show_urls --no-color | grep '" what "'"))
	(buf (concat "*URL<" what ">")))
    ))
