;;; package --- init.el
;;; Commentary:

;; A new init.el

;;; Code:

(with-eval-after-load 'package
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t))

;; Some testing libraries
(cond ((file-exists-p "/home/matias/.emacs.d/libs/")
       (progn
         (add-to-list 'load-path "/home/matias/.emacs.d/libs/")
         (defun load-optional-libraries ()
           "Load testing/optional libraries defined on `peke-optional.el'"
           (interactive)
           (require 'peke-optional)))))

;; Automatic reread from disk if the underlying file changes
(setq auto-revert-interval 1)
(setq auto-revert-check-vc-info t)
(global-auto-revert-mode)

;; Fix archaic defaults
(setq sentence-end-double-space nil)

;; Make right click do something sensible
(when (display-graphic-p)
  (context-menu-mode))

;; Tramp - Faster than default scp
(setq tramp-default-method "ssh")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Utility functions
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun append-to-list (listname elements)
  "Append all `ELEMENTS' to `LISTNAME'

Example
  (append-to-list 'listname '(val1 val2))
Is the same as
  (add-to-list 'listname 'val1)
  (add-to-list 'listname 'val2)"
  (mapcar (lambda (elem)
            (add-to-list listname elem))
          elements))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Interface enhancements/defaults
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Mode line information
(setq line-number-mode t)                        ; Show current line in modeline
(setq column-number-mode t)                      ; Show column as well

(setq x-underline-at-descent-line nil)           ; Prettier underlines
(setq switch-to-buffer-obey-display-actions t)   ; Make switching buffers more consistent

(setq-default show-trailing-whitespace nil)      ; By default, don't underline trailing spaces
(setq-default indicate-buffer-boundaries nil)    ; Show buffer top and bottom in the margin

;; Misc. UI tweaks
(blink-cursor-mode -1)                                ; Steady cursor
(pixel-scroll-precision-mode)                         ; Smooth scrolling

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Tab-bar configuration
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Show the tab-bar as soon as tab-bar functions are invoked
(setq tab-bar-show 1)

;; Add the time to the tab-bar, if visible
(add-to-list 'tab-bar-format 'tab-bar-format-align-right 'append)
(add-to-list 'tab-bar-format 'tab-bar-format-global 'append)
(setq display-time-format "%a %F %T")
(setq display-time-interval 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Theme
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package emacs
  :config
  (setq ring-bell-function 'ignore
        use-package-compute-statistics t)
  (if (display-graphic-p)
      (progn
        ;(set-face-attribute 'default nil :font "Roboto Mono" :height 100)
        ; (set-face-attribute 'variable-pitch nil :font "ETBembo")
        (custom-theme-set-faces
         'user
         '(variable-pitch ((t (:family "ETBembo" :height 120))))
         '(fixed-pitch ((t (:family "Monospace" :height 100))))
         ;; This is for the modeline?
         ; '(warning ((t :weight regular :foreground "OrangeRed1")))
         )
        ;; Colorscheme
        (setq modus-themes-operandi-color-overrides
	      '((bg-main . "#fefcf4")))
        ;(load-theme 'spacemacs-dark t)
        (setq-default cursor-type 'bar)
        (blink-cursor-mode t)
        (let ((bg (face-attribute 'mode-line :background)))
          (set-face-attribute 'mode-line nil
                              :box (list :line-width 1 :color "gray60" :style nil)
                              :background bg))
        (load-theme 'leuven t))
    (load-theme 'modus-vivendi))
  (add-hook 'compilation-filter-hook 'ansi-color-compilation-filter)
  ;; Treesitter config

  ;; Tell Emacs to prefer the treesitter mode
  ;; You'll want to run the command `M-x
  ;; treesit-install-language-grammar' before editing.
  (setq major-mode-remap-alist
        '(
          ;; (yaml-mode . yaml-ts-mode)
          (bash-mode . bash-ts-mode)
          (js2-mode . js-ts-mode)
          (typescript-mode . typescript-ts-mode)
          (json-mode . json-ts-mode)
          (css-mode . css-ts-mode)
          (python-mode . python-ts-mode)
          (perl-mode . cperl-mode)))
  :config

  ;; :hook
  ;; Auto parenthesis matching
  ;; ((prog-mode . electric-pair-mode))
  :hook
  ((emacs-lisp-mode . enable-paredit-mode)
   ;; (eval-expression-minibuffer-setup . enable-paredit-mode)
   (ielm-mode . enable-paredit-mode)
   (lisp-mode . enable-paredit-mode)
   (lisp-interaction-mode . enable-paredit-mode)
   (scheme-mode . enable-paredit-mode)
   (slime-repl-mode . enable-paredit-mode)
   (scheme-mode . rainbow-delimiters-mode)))

(defun my-c-mode-common-hook ()
  ;; my customizations for all of c-mode, c++-mode, objc-mode, java-mode
  (c-set-offset 'substatement-open 0)
  ;; other customizations can go here

  (setq c++-tab-always-indent t)
  (setq c-basic-offset 4)                  ;; Default is 2
  (setq c-indent-level 4)                  ;; Default is 2

  (setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60))
  (setq tab-width 4)
  (setq indent-tabs-mode nil)  ; use spaces only if nil
  )

(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;; (use-package company-qml
;;   :defer t
;;   :config
;;   (add-to-list 'company-backends 'company-qml))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Org
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Packages for org
(use-package olivetti
  :ensure t)

(use-package org-journal
  :ensure t
  :defer t
  :bind (("C-c j t" . my/find-journal-today))
  :init
  (setq org-journal-prefix-key "C-c j ")
  :config
  (setq org-journal-dir "~/Documents/roam/journal/"
        org-journal-date-format "%A, %d %B %Y"
        ;; Find files of org journal in the agenda files
        org-agenda-file-regexp "\\`\\\([^.].*\\.org\\\|[0-9]\\\{8\\\}\\\(\\.gpg\\\)?\\\)\\'"
        org-journal-enable-agenda-integration t
        org-journal-file-type 'weekly)
  (add-to-list 'org-agenda-files org-journal-dir)
  :preface
  (defun my/find-journal-today ()
    (interactive)
    (org-journal-new-entry t)))

(use-package org
  :hook ((org-mode . visual-line-mode)  ; wrap lines at word breaks
         (org-mode . flyspell-mode))    ; spell checking!

  :bind (:map global-map
              ("C-c l s" . org-store-link)          ; Mnemonic: link → store
              ("C-c l i" . org-insert-link-global)  ; Mnemonic: link → insert
	      ("C-c a" . org-agenda))
  :config
  (require 'org-tempo)			; Abbrv support for code blocks
  (require 'org-agenda)
  (add-to-list 'org-export-backends 'md)

  ;; Make org-open-at-point follow file links in the same window
  (setf (cdr (assoc 'file org-link-frame-setup)) 'find-file)

  ;; Make exporting quotes better
  (setq org-export-with-smart-quotes t)
  ;; Dont prompt for evaluation
  (setq org-confirm-babel-evaluate nil)
  ;; This variable hides bold, italics, etc. markers
  (setq org-hide-emphasis-markers t)

  ;; This things are fixed instead of variable
  (custom-theme-set-faces
   'user
   '(org-block ((t (:inherit fixed-pitch))))
   '(org-code ((t (:inherit fixed-pitch))))
   '(org-document-info ((t (:foreground "dark orange"))))
   '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
   '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
   '(org-link ((t (:foreground "royal blue" :underline t))))
   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-property-value ((t (:inherit fixed-pitch))) t)
   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-table ((t (:inherit fixed-pitch))))
   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold))))
   '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))
  (org-babel-do-load-languages 'org-babel-load-languages '((shell . t)
							   (sql . t)
							   (sqlite . t)
							   (restclient . t)
							   (python . t)
							   (ruby . t))))

(add-hook 'org-mode-hook (lambda ()
			   (auto-fill-mode)
			   (variable-pitch-mode)
			   (bug-reference-mode)
			   ;(olivetti-mode)
			   ;(olivetti-set-width 100)
                           ))
(use-package org-roam
  :ensure t
  :defer t
  :init (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/Documents/roam")
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert))
  :config
  ;; (org-roam-setup) ;; One shot?
  (org-roam-db-autosync-mode)
  (setq org-roam-index-file "~/Documents/roam/index.org")
  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-side-window)
                 (side . right)
                 (window-width . 0.4)
                 (window-height . fit-window-to-buffer)))
  ;; Better python identation
  (setq org-edit-src-content-indentation 0))

(use-package org-modern
  :defer t
  :hook ((org-mode . org-modern-mode))
  :config
  (setq org-tags-column 0
        org-auto-align-tags nil
        org-catch-invisible-edits 'show-and-error
        org-insert-heading-respect-content t
        org-hide-emphasis-markers t
        org-pretty-entities t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Minibuffer and completion
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Vertico: better vertical completion for minibuffer commands
(use-package vertico
  :ensure t
  :bind
  (:map vertico-map
	("<tab>" . vertico-insert))

  :init
  ;; You'll want to make sure that e.g. fido-mode isn't enabled
  (vertico-mode))

(use-package vertico-posframe
  :ensure t
  :hook (after-init . vertico-posframe-mode)
  :custom
  (vertico-posframe-parameters
   '((left-fringe . 8)
     (right-fringe . 8))))

;; Marginalia: annotations for minibuffer
(use-package marginalia
  :ensure t
  :config
  (marginalia-mode))

;; (use-package company
;;   :ensure t
;;   :hook ((prog-mode . company-mode)
;;          (comint-mode . company-mode))
;;   :bind
;;   (:map company-mode-map
;; 	("C-;" . company-complete))
;;   :config
;;   ;; Don't trigger completion automaticly
;;   (setq company-idle-delay nil
;;         company-global-modes '(prog-mode org-mode lisp-mode elisp-mode emacs-lisp-mode sly-repl-mode)))

(use-package corfu
  :defer t
  :hook (after-init . global-corfu-mode)
  :bind (("C-;" . completion-at-point)))

;; Orderless: powerful completion style
(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless basic)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Packages
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Define some aliases for eshell
(defun eshell/open (file)
  (find-file file))

;; Treesiter config
(use-package emacs

  )

;; Eglot
(use-package eglot
  :defer t
  :custom
  (eglot-send-changes-idle-time 0.1)
  (eglot-events-buffer-size 1000)
  (eglot-report-progress nil)
  :config
  (fset #'jsonrpc--log-event #'ignore)  ; massive perf boost---don't log every event
  ;; Sometimes you need to tell Eglot where to find the language server
  (add-to-list 'eglot-server-programs
               '(qml-mode . ("qmlls6")))
  (add-to-list 'eglot-server-programs
               '(c++-mode . ("clangd" "-log=error" "--clang-tidy" "--background-index" "--limit-results=500"
                             "--completion-style=bundled")))
  )


;; Flymake
(use-package flymake
  :defer t
  :hook ((prog-mode . flymake-mode)))

;; YAML
(use-package yaml-mode :ensure t)

(use-package outline-indent
  :defer t
  :hook ((yaml-mode . outline-indent-minor-mode)
         (yaml-ts-mode . outline-indent-minor-mode))
  :custom
  (outline-indent-shift-width 2))

(use-package qml-mode
  :defer t)

;; JSON
(use-package json-mode
  :ensure t
  :bind
  (("C-c j p" . json-pretty-print-buffer)))

;; Lisp
;; (use-package sly :defer t :ensure t)
(use-package slime
  :ensure t
  :defer t)
(load (expand-file-name "~/quicklisp/slime-helper.el"))
(setq inferior-lisp-program "sbcl")

;; (use-package slime-company
;;   :ensure t
;;   :after (slime company)
;;   :config
(setq slime-company-completion 'fuzzy
      slime-company-after-completion 'slime-company-just-one-space)
(slime-setup '(slime-fancy slime-company))
;; (add-to-list 'load-path "/home/matias/Workspace/swank-chicken/")
;; (autoload 'chicken-slime "chicken-slime" "SWANK backend for Chicken" t)
;; (setq slime-csi-path "/home/matias/.local/bin/csi"
;;       swank-chicken-port 4015)
;; (add-hook 'scheme-mode-hook (lambda () (slime-mode t)))

;; Indenting module body code at column 0
(defun scheme-module-indent (state indent-point normal-indent) 0)
(put 'module 'scheme-indent-function 'scheme-module-indent)

(put 'and-let* 'scheme-indent-function 1)
(put 'parameterize 'scheme-indent-function 1)
(put 'handle-exceptions 'scheme-indent-function 1)
(put 'when 'scheme-indent-function 1)
(put 'unless 'scheme-indent-function 1)
(put 'match 'scheme-indent-function 1)


;; Scheme
;; (use-package geiser-chicken
;;   :ensure t)


;; Racket
(use-package racket-mode
  :defer t)

(use-package rainbow-delimiters
  :ensure t
  :hook ((lisp-mode . rainbow-delimiters-mode)))

;; Jenkinsfile
(use-package jenkinsfile-mode
  :ensure t)
(setq-default indent-tabs-mode nil)

;; Rest client
(use-package restclient :defer t :ensure t)

;; Magit
(use-package magit
  :ensure t
  :bind (("s-g" . magit-status)
	 ("C-c g" . magit-status)))

;; Forge
(use-package forge
  :after magit
  :ensure t)

;; python-mode configuration

(setq python-indent-guess-indent-offset-verbose nil)

(use-package pyvenv
  :ensure t
  :defer t
  :config
  ;; Set correct Python interpreter
  (setq pyvenv-post-activate-hooks
	(list (lambda ()
		(setq python-shell-interpreter (concat pyvenv-virtual-env "bin/python3")))))
  (setq pyvenv-post-deactivate-hooks
	(list (lambda ()
		(setq python-shell-interpreter "python3")))))

(use-package python-pytest
  :defer t
  :ensure t
  :config
  (transient-append-suffix
    'python-pytest-dispatch
    "-v"
    '("--db" "Create db" "--create-db")))

;; Perl
(with-eval-after-load 'cperl
  (setq cperl-indent-parens-as-block t
        cperl-close-paren-offset (- cperl-indent-level)))

;; Load path from shell environment
;; (use-package exec-path-from-shell :ensure t :defer t)

(add-to-list 'exec-path "/home/matias/.local/share/gem/ruby/3.0.0/bin")

(use-package cmake-project
  :defer t
  :preface
  (defun maybe-cmake-project-mode ()
    (when (or (file-exists-p "CMakeLists.txt")
            (file-exists-p (expand-file-name "CMakeLists.txt" (projectile-project-root))))
        (cmake-project-mode)))
  :hook
  (c-mode . maybe-cmake-project-mode)
  (c++-mode . maybe-cmake-project-mode))


;; sh-mode configuration
(use-package flymake-shellcheck
  :commands flymake-shellcheck-load
  :init
  (add-hook 'sh-mode-hook 'flymake-shellcheck-load))

(use-package nerd-icons
  :defer t
  :ensure t)

(use-package doom-modeline
  :ensure t
  :hook (after-init . doom-modeline-mode)
  :config
  (set-face-attribute 'doom-modeline-warning nil :weight 'regular :foreground "OrangeRed2")
  (set-face-attribute 'doom-modeline-urgent nil :weight 'regular)
  (set-face-attribute 'doom-modeline-info nil :weight 'regular)
  (set-face-attribute 'doom-modeline-buffer-file nil :weight 'regular)
  (set-face-attribute 'doom-modeline-buffer-modified nil :weight 'semi-bold :foreground "black")
  (set-face-attribute 'doom-modeline-buffer-major-mode nil :weight 'regular :italic t)
  :custom
  (doom-modeline-buffer-encoding nil)
  (doom-modeline-minor-modes nil)
  (doom-modeline-buffer-modification-icon t)
  (doom-modeline-time nil)
  (doom-modeline-vcs-max-length 35)
  (doom-modeline-env-python-executable "python"))

;; This will echo the diagnostics in the minibuffer
(setq lsp-diagnostics-provider :flymake)

;; IMenu everywhere
(use-package imenu-anywhere
  :ensure t
  :bind (("C-." . imenu-anywhere)))

(defcustom ignore-directories '("venv" "__pycache__" "*.egg-info")
  "Directories to be ignored by `projectile', `treemacs', etc."
  :group 'peke
  :type '(repeat string))

(defcustom ignore-files '()
  "Files to be ignored by `projectile', `treemacs', etc."
  :group 'peke
  :type '(repeat string))

;; Projectile
(use-package projectile
  :hook (after-init . projectile-mode)
  :preface
  (defun ignored-projectile-generic-command ()
    (cond
     (projectile-fd-executable
      (let ((exclude-flags (mapcar (lambda (dir) (format "-E %s" dir))
                                   ignore-directories)))
       (format "%s . -0 --type f --color=never %s --strip-cwd-prefix"
               projectile-fd-executable
               (string-join exclude-flags " "))))
     (t projectile-generic-comand)))
  :config
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  :custom
  (projectile-indexing-method 'hybrid)
  (projectile-sort-order 'modification-time)
  (projectile-use-git-grep t)
  (projectile-generic-command (ignored-projectile-generic-command)))

(use-package treemacs
  :ensure t
  :preface
  (defun peke/treemacs-ignore-file-p (filename absolute-path)
    ;; TODO: Terminar esto con los defcustom de mas arria
    (string-equal "venv" filename))
  :bind
  ("<f5>" . treemacs)
  ("M-0" . treemacs-select-window)
  :custom
  (treemacs-is-never-other-window t)
  (treemacs-space-between-root-nodes nil)
  :config
  (add-to-list 'treemacs-ignored-file-predicates #'peke/treemacs-ignore-file-p)
  :hook
  (treemacs-mode . treemacs-project-follow-mode))

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

(use-package treemacs-persp ;;treemacs-perspective if you use perspective.el vs. persp-mode
  :after (treemacs persp-mode) ;;or perspective vs. persp-mode
  :ensure t
  :config (treemacs-set-scope-type 'Perspectives))

(use-package treemacs-all-the-icons
  :ensure t
  :after (treemacs)
  :config (treemacs-load-all-the-icons-with-workaround-font "Hermit"))


;; (use-package treemacs-tab-bar ;;treemacs-tab-bar if you use tab-bar-mode
;;   :after (treemacs)
;;   :ensure t
;;   :config (treemacs-set-scope-type 'Tabs))

(use-package all-the-icons
  :ensure t
  :if (display-graphic-p))

;; Window purpose
(use-package window-purpose
  :ensure t
  :defer t
  :hook (after-init . purpose-mode)
  :config
  (add-to-list 'purpose-user-mode-purposes '(python-mode . py))
  (add-to-list 'purpose-user-mode-purposes '(inferior-python-mode . py-repl))
  (purpose-compile-user-configuration))

(use-package helpful
  :ensure t
  :defer t
  :bind (:map global-map
              ("C-h f" . #'helpful-callable)
              ("C-h v" . #'helpful-variable) ; Mnemonic: link → insert
	      ("C-h k" . #'helpful-key)
              ("C-h x" . #'helpful-command)))

(use-package perspective
  :defer t
  :ensure t
  :hook (after-init . persp-mode)
  :bind
  ("C-x C-b" . persp-list-buffers)         ; or use a nicer switcher, see below
  :custom
  (persp-mode-prefix-key (kbd "C-c M-p"))  ; pick your own prefix key here
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Custom functions
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun my/reload-dir-locals ()
  (interactive)
  (let ((enable-local-variables :all))
    (hack-dir-local-variables-non-file-buffer)))

(defun my/find-emacs-init ()
  "`'find-file' to `emacs' init file."
  (interactive)
  (find-file "~/.emacs.d/init.el"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Global keybindigns
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-key global-map (kbd "RET") 'newline-and-indent)
;(global-set-key (kbd "C-x C-b") 'ibuffer)
; (global-set-key (kbd "C-;") 'completion-at-point)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "M-z") 'zap-up-to-char)
(global-set-key (kbd "C-.") #'imenu-anywhere)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "C-c m i") 'my/find-emacs-init)

(provide 'init)
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("cdad4e5bc718111deb1f1a2a35e9781e11724144a1eb2064732c337160946760" "cfb0bb44259cf576124b900613628f4b589df1c3860185d24fffde6666bc88cf" "7fd8b914e340283c189980cd1883dbdef67080ad1a3a9cc3df864ca53bdc89cf" "f3781be0be23cc71c89b317489e07a4ad3e885f84c0a618692b53bbd69e60843" "9bed3fb18c8d18acca50f3609efcaa8d1c72027802cec8d291d4577e3e48b825" "871b064b53235facde040f6bdfa28d03d9f4b966d8ce28fb1725313731a2bcc8" "d445c7b530713eac282ecdeea07a8fa59692c83045bf84dd112dd738c7bcad1d" "48ea73e4e893ae318e4b0e3dda47ba78e322779a2107fa1aa9a71380590c03c5" "3770d0ae70172461ee0a02edcff71b7d480dc54066e8960d8de9367d12171efb" "835d934a930142d408a50b27ed371ba3a9c5a30286297743b0d488e94b225c5f" "98ef36d4487bf5e816f89b1b1240d45755ec382c7029302f36ca6626faf44bbd" "7b8f5bbdc7c316ee62f271acf6bcd0e0b8a272fdffe908f8c920b0ba34871d98" "de8f2d8b64627535871495d6fe65b7d0070c4a1eb51550ce258cd240ff9394b0" "1781e8bccbd8869472c09b744899ff4174d23e4f7517b8a6c721100288311fa5" "e7820b899036ae7e966dcaaec29fd6b87aef253748b7de09e74fdc54407a7a02" "7e0dec7d9f3821acba56ab19083ed059a94753ed79ee89b0d8167b727ed6cb81" "bbb13492a15c3258f29c21d251da1e62f1abb8bbd492386a673dcfab474186af" "a5270d86fac30303c5910be7403467662d7601b821af2ff0c4eb181153ebfc0a" "ba323a013c25b355eb9a0550541573d535831c557674c8d59b9ac6aa720c21d3" default))
 '(eglot-events-buffer-size 0 nil nil "Customized with use-package eglot")
 '(eldoc-echo-area-use-multiline-p nil)
 '(package-selected-packages
   '(ob-graphql graphql-mode standard-themes leuven-theme outline-indent corfu company-qml qml-mode cmake-project eldoc-overlay kanagawa-theme nano-theme theme-looper org-bullets spacemacs-theme perspective flycheck slime-company gruvbox-theme paredit slime acme-theme plan9-theme racket-mode rainbow-delimiters helpful frame-mode cmake-mode csv-mode ob-restclient daemons yaml-mode window-purpose vertico-posframe treemacs restclient pyvenv python-pytest projectile-rails org-roam org-modern org-journal orderless olivetti marginalia json-mode jenkinsfile-mode imenu-anywhere forge doom-modeline company all-the-icons))
 '(safe-local-variable-values
   '((projectile-project-run-cmd . "./build/bin/kode-reviewer")
     (projectile-project-run-cmd . "./build/bin/kodereviewer")
     (projectile-project-compilation-cmd . "cmake -DCMAKE_BUILD_TYPE=Debug --build build -j8")
     (projectile-project-compilation-cmd . "cmake  --build build -j8")
     (projectile-project-compilation-cmd . "ccmake --build build -j8")
     (projectile-project-compilation-cmd . "cmake --build build -j8")
     (projectile-project-compilation-cmd "cmake --build build -j8")
     (projectile-project-run-cmd . "./build/bp"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fixed-pitch ((t (:family "Monospace" :height 100))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit fixed-pitch))))
 '(org-document-info ((t (:foreground "dark orange"))))
 '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-link ((t (:foreground "royal blue" :underline t))))
 '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))))
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
 '(variable-pitch ((t (:family "ETBembo" :height 120)))))
