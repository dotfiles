;;; package --- custom-org-mode
;;; Commentary:
;; This is a package to setup an orgmode editor.

;;; Code:
;; Setup gargabe collection
(setq gc-cons-threshold (* 80 1000 1000))

(add-to-list 'load-path "~/.emacs.d/libs/nano-emacs")

(setq nano-font-family-monospaced "Roboto Mono")
(setq nano-font-family-proportional "Roboto Regular")
(setq nano-font-size 10)

(require 'nano-layout)
(require 'nano-theme-light)
(require 'nano-faces)
(nano-faces)
(require 'nano-theme)
(nano-theme)

(require 'nano-defaults)
;; Nano header & mode lines (optional)
(require 'nano-modeline)

;; Nano key bindings modification (optional)
(require 'nano-bindings)

;; Welcome message (optional)
(let ((inhibit-message t))
  (message "Welcome to GNU Emacs / N Λ N O edition")
  (message (format "Initialization time: %s" (emacs-init-time))))

;; (require 'nano-base-colors)


(require 'package)
;(require 'use-package)
;; Use https if enabled and add melpa.
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
		       (not (gnutls-available-p))))
	  (proto (if no-ssl "http" "https")))
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (package-initialize))

(use-package org-roam
  :ensure t
  :defer t
  :init (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/OrgRoam")
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert))
  :config
  (org-roam-setup))
(provide 'custom-org-mode)
;;; custom-org-mode.el ends here
