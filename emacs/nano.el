(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
		       (not (gnutls-available-p))))
	  (proto (if no-ssl "http" "https")))
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (package-initialize))
(add-to-list 'load-path "~/.emacs.d/libs/nano-emacs")
;; OrgMode
(add-hook 'org-mode-hook (lambda ()
			   (auto-fill-mode)
			   ;;(variable-pitch-mode)
			   (bug-reference-mode)))
(setq org-edit-src-content-indentation 0)
(use-package org-roam
  :ensure t
  :defer t
  :init (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/OrgRoam")
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert))
  :config
  (org-roam-setup))
(use-package org-modern
  :ensure t
  :defer t
  :init (global-org-modern-mode))
(setq org-confirm-babel-evaluate nil)


(setq nano-font-family-monospaced "Roboto Mono")

(load "~/.emacs.d/libs/nano-emacs/nano.el")
