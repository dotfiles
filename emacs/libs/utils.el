;;; utils.el --- Utility package
;;; Commentary:
;; None

;;; Code:
(require 'org-table)

(defun csv-to-table (file)
  "Turn a csv with ; separator in FILE to table."
  (with-temp-buffer
    (erase-buffer)
    (insert-file-contents file nil 0 500)
    (org-table-convert-region (point-min) (point-max) ";")
    (buffer-string)))

(provide 'utils)
;;; utils.el ends here
