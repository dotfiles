;;; penumbra-theme.el --- A color theme based on Acme & Sam from Plan 9  -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Ian Yi-En Pan

;; Author: Ian Y.E. Pan
;; URL: https://github.com/ianpan870102/acme-emacs-theme
;; Version: 1.0.0

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; A color theme for Emacs based on Acme & Sam from Plan 9

;;; Credits:
;; This theme was modified from John Louis Del Rosario's plan9-theme.el

;;; Code:

(defgroup penumbra-theme nil
  "Options for penumbra theme."
  :group 'faces)

(defcustom penumbra-theme-black-fg nil
  "If non-nil, foreground will be pure black instead of the default dark grey."
  :group 'penumbra-theme
  :type 'boolean)

(deftheme penumbra "A color theme based on Penumbra & Sam")

;;; Color palette

(let ((class '((class color) (min-colors 89)))
      (bg              "#FFF7ED") ; default bg
      (bg-alt          "#FFFDFB")
      (bg-dark         "#F2E6D4")
      (fg              (if penumbra-theme-black-fg "#000000" "#24272B")) ; default fg
      (fg-alt          "#B8B09A")
      (fg-dark         "#988D6D")
      (fg-light        "#CCCCB7")
      (highlight       "#E8EB98")
      (highlight-alt   "#E8EBC8")

      ;; Standardized palette
      (penumbra-cyan            "#00B3C2")
      (penumbra-cyan-light      "#00C4D7")
      (penumbra-red             "#DF7F78")
      (penumbra-red-light       "#F58C81")
      (penumbra-yellow          "#9CA748")
      (penumbra-yellow-light    "#A9B852")
      (penumbra-green           "#50B584")
      (penumbra-green-alt       "#54C794")
      (penumbra-green-light     "#54C794")
      (penumbra-blue            "#61A3E6")
      (penumbra-blue-light      "#6EB2FD")
      (penumbra-purple          "#A48FE1")
      (penumbra-purple-light    "#B69CF6"))

;;; Theme Faces
  (custom-theme-set-faces
   'penumbra

;;;; Built-in

;;;;; basic coloring
   `(button                                       ((t (:underline t))))
   `(link                                         ((t (:foreground "#0066cc":weight normal))))
   `(highlight                                    ((t (:inherit link :underline t)))) ; link hover
   `(link-visited                                 ((t (:foreground ,penumbra-purple :underline t :weight normal))))
   `(default                                      ((t (:foreground ,fg :background ,bg))))
   `(cursor                                       ((t (:foreground ,bg :background ,fg))))
   `(escape-glyph                                 ((t (:foreground ,penumbra-cyan-light :bold nil))))
   `(fringe                                       ((t (:foreground ,fg :background ,bg))))
   `(line-number                                  ((t (:foreground ,fg :background ,bg-alt))))
   `(line-number-current-line                     ((t (:foreground ,fg :background ,bg-alt))))
   `(header-line                                  ((t (:foreground ,fg :background ,penumbra-blue-light :box t))))
   `(success                                      ((t (:foreground ,penumbra-green :weight normal))))
   `(warning                                      ((t (:foreground ,penumbra-red :weight normal))))
   `(error                                        ((t (:foreground ,penumbra-red :bold t))))

;;;;; compilation
   `(compilation-column-face                      ((t (:foreground ,penumbra-yellow :background ,penumbra-yellow-light))))
   `(compilation-column-number                    ((t (:foreground ,penumbra-yellow :background ,penumbra-yellow-light))))
   `(compilation-error-face                       ((t (:foreground ,penumbra-red :weight normal :underline t))))
   `(compilation-face                             ((t (:foreground ,fg))))
   `(compilation-info-face                        ((t (:foreground ,penumbra-blue))))
   `(compilation-info                             ((t (:foreground ,penumbra-blue :underline t))))
   `(compilation-line-face                        ((t (:foreground ,penumbra-purple))))
   `(compilation-line-number                      ((t (:foreground ,penumbra-yellow :background ,penumbra-yellow-light))))
   `(compilation-message-face                     ((t (:foreground ,penumbra-blue))))
   `(compilation-warning-face                     ((t (:foreground ,penumbra-yellow :weight normal :underline t))))
   `(compilation-mode-line-exit                   ((t (:foreground ,penumbra-cyan :weight normal))))
   `(compilation-mode-line-fail                   ((t (:foreground ,penumbra-red :weight normal))))
   `(compilation-mode-line-run                    ((t (:foreground ,penumbra-purple :weight normal))))

;;;;; grep
   `(grep-context-face                            ((t (:foreground ,fg-alt))))
   `(grep-error-face                              ((t (:foreground ,penumbra-red :weight normal :underline t))))
   `(grep-hit-face                                ((t (:foreground ,penumbra-purple :weight normal))))
   `(grep-match-face                              ((t (:foreground ,penumbra-cyan :weight normal))))
   `(match                                        ((t (:background ,penumbra-cyan :foreground ,penumbra-cyan-light))))

;;;;; ag
   `(ag-hit-face                                  ((t (:foreground ,penumbra-green :weight normal))))
   `(ag-match-face                                ((t (:foreground ,penumbra-cyan :background ,penumbra-cyan-light :weight normal))))

;;;;; isearch
   `(isearch                                      ((t (:foreground ,fg :weight normal :background ,penumbra-cyan-light))))
   `(isearch-fail                                 ((t (:foreground ,fg :weight normal :background ,penumbra-red))))
   `(lazy-highlight                               ((t (:foreground ,fg :weight normal :background ,penumbra-blue-light))))

   `(menu                                         ((t (:foreground ,bg :background ,fg))))
   `(minibuffer-prompt                            ((t (:foreground ,fg :weight normal))))
   `(region                                       ((,class (:foreground ,fg :background ,highlight :extend nil))))
   `(secondary-selection                          ((t (:background ,penumbra-green-light))))
   `(trailing-whitespace                          ((t (:background ,penumbra-red-light))))
   `(vertical-border                              ((t (:foreground ,penumbra-cyan))))

;;;;; font lock
   `(font-lock-builtin-face                       ((t (:foreground ,fg :weight normal))))
   `(font-lock-function-name-face                 ((t (:foreground ,fg :weight normal))))
   `(font-lock-string-face                        ((t (:foreground ,penumbra-red))))
   `(font-lock-keyword-face                       ((t (:foreground ,penumbra-blue :weight bold)))) ; if, else, for, while, return...
   `(font-lock-type-face                          ((t (:foreground ,fg :weight bold)))) ; int, float, string, void...
   `(font-lock-constant-face                      ((t (:foreground ,fg :weight bold)))) ; NULL, nullptr, true, false...
   `(font-lock-variable-name-face                 ((t (:foreground ,fg :weight normal))))
   `(font-lock-comment-face                       ((t (:foreground ,penumbra-green :italic nil))))
   `(font-lock-comment-delimiter-face             ((t (:foreground ,penumbra-green :italic nil))))
   `(font-lock-doc-face                           ((t (:foreground ,penumbra-yellow :italic nil))))
   `(font-lock-negation-char-face                 ((t (:foreground ,penumbra-red :weight normal))))
   `(font-lock-preprocessor-face                  ((t (:foreground ,penumbra-red :weight normal))))
   `(font-lock-regexp-grouping-construct          ((t (:foreground ,penumbra-purple :weight normal))))
   `(font-lock-regexp-grouping-backslash          ((t (:foreground ,penumbra-purple :weight normal))))
   `(font-lock-warning-face                       ((t (:foreground ,penumbra-red :weight normal))))

;;;;; table
   `(table-cell                                   ((t (:background ,bg-alt))))

;;;;; ledger
   `(ledger-font-directive-face                   ((t (:foreground ,penumbra-cyan))))
   `(ledger-font-periodic-xact-face               ((t (:inherit ledger-font-directive-face))))
   `(ledger-font-posting-account-face             ((t (:foreground ,penumbra-blue))))
   `(ledger-font-posting-amount-face              ((t (:foreground ,penumbra-red))))
   `(ledger-font-posting-date-face                ((t (:foreground ,penumbra-red :weight normal))))
   `(ledger-font-payee-uncleared-face             ((t (:foreground ,penumbra-purple))))
   `(ledger-font-payee-cleared-face               ((t (:foreground ,fg))))
   `(ledger-font-payee-pending-face               ((t (:foreground ,penumbra-yellow))))
   `(ledger-font-xact-highlight-face              ((t (:background ,bg-alt))))

;;;; Third-party


;;;;; anzu
   `(anzu-mode-line                               ((t (:foreground ,penumbra-yellow :background ,penumbra-yellow-light :weight normal))))

;;;;; clojure-mode
   `(clojure-interop-method-face                  ((t (:inherit font-lock-function-name-face))))

;;;;; clojure-test-mode
   `(clojure-test-failure-face                    ((t (:foreground ,penumbra-red :weight normal :underline t))))
   `(clojure-test-error-face                      ((t (:foreground ,penumbra-red :weight normal :underline t))))
   `(clojure-test-success-face                    ((t (:foreground ,penumbra-green :weight normal :underline t))))

;;;;; diff
   `(diff-added                                   ((,class (:foreground ,fg :background ,penumbra-green-light))
                                                   (t (:foreground ,fg :background ,penumbra-green-light))))
   `(diff-changed                                 ((t (:foreground ,penumbra-yellow))))
   `(diff-context                                 ((t (:foreground ,fg))))
   `(diff-removed                                 ((,class (:foreground ,fg :background ,penumbra-red-light))
                                                   (t (:foreground ,fg :background ,penumbra-red-light))))
   `(diff-refine-added                            ((t :inherit diff-added :background ,penumbra-green-light :weight bold :underline t)))
   `(diff-refine-change                           ((t :inherit diff-changed :weight normal)))
   `(diff-refine-removed                          ((t :inherit diff-removed :background ,penumbra-red-light :weight bold :underline t)))
   `(diff-header                                  ((,class (:foreground ,fg :weight normal))
                                                   (t (:foreground ,penumbra-purple-light :weight normal))))
   `(diff-file-header                             ((,class (:foreground ,fg :background ,penumbra-cyan-light :weight normal))
                                                   (t (:foreground ,fg :background ,penumbra-cyan-light :weight normal))))
   `(diff-hunk-header                             ((,class (:foreground ,penumbra-green :weight normal))
                                                   (t (:foreground ,penumbra-green :weight normal))))
;;;;; dired/dired+/dired-subtree
   `(dired-directory                              ((t (:foreground ,penumbra-blue :weight bold))))
   `(diredp-display-msg                           ((t (:foreground ,penumbra-blue))))
   `(diredp-compressed-file-suffix                ((t (:foreground ,penumbra-purple))))
   `(diredp-date-time                             ((t (:foreground ,penumbra-green))))
   `(diredp-deletion                              ((t (:foreground ,penumbra-red))))
   `(diredp-deletion-file-name                    ((t (:foreground ,penumbra-red))))
   `(diredp-dir-heading                           ((t (:foreground ,penumbra-blue :background ,penumbra-blue-light :weight bold))))
   `(diredp-dir-priv                              ((t (:foreground ,penumbra-blue))))
   `(diredp-exec-priv                             ((t (:foreground ,penumbra-yellow))))
   `(diredp-executable-tag                        ((t (:foreground ,penumbra-yellow))))
   `(diredp-file-name                             ((t (:foreground ,fg))))
   `(diredp-file-suffix                           ((t (:foreground ,penumbra-yellow))))
   `(diredp-flag-mark                             ((t (:foreground ,penumbra-cyan))))
   `(diredp-flag-mark-line                        ((t (:foreground ,penumbra-cyan))))
   `(diredp-ignored-file-name                     ((t (:foreground ,fg-light))))
   `(diredp-link-priv                             ((t (:foreground ,penumbra-purple))))
   `(diredp-mode-line-flagged                     ((t (:foreground ,penumbra-yellow))))
   `(diredp-mode-line-marked                      ((t (:foreground ,penumbra-yellow))))
   `(diredp-no-priv                               ((t (:foreground ,fg))))
   `(diredp-number                                ((t (:foreground ,penumbra-blue))))
   `(diredp-other-priv                            ((t (:foreground ,fg))))
   `(diredp-rare-priv                             ((t (:foreground ,fg))))
   `(diredp-read-priv                             ((t (:foreground ,fg))))
   `(diredp-symlink                               ((t (:foreground ,fg :background ,penumbra-blue-light))))
   `(diredp-write-priv                            ((t (:foreground ,fg))))
   `(diredp-dir-name                              ((t (:foreground ,penumbra-blue :weight bold))))
   `(dired-subtree-depth-1-face                   ((t (:background ,bg))))
   `(dired-subtree-depth-2-face                   ((t (:background ,bg))))
   `(dired-subtree-depth-3-face                   ((t (:background ,bg))))

;;;;; elfeed
   `(elfeed-search-date-face                      ((t (:foreground ,penumbra-blue))))
   `(elfeed-search-title-face                     ((t (:foreground ,fg))))
   `(elfeed-search-unread-title-face              ((t (:foreground ,fg))))
   `(elfeed-search-feed-face                      ((t (:foreground ,penumbra-green))))
   `(elfeed-search-tag-face                       ((t (:foreground ,penumbra-red))))
   `(elfeed-search-unread-count-face              ((t (:foreground ,fg))))

;;;;; erc
   `(erc-default-face                             ((t (:foreground ,fg))))
   `(erc-header-line                              ((t (:inherit header-line))))
   `(erc-action-face                              ((t (:inherit erc-default-face))))
   `(erc-bold-face                                ((t (:inherit erc-default-face :weight normal))))
   `(erc-underline-face                           ((t (:underline t))))
   `(erc-error-face                               ((t (:inherit font-lock-warning-face))))
   `(erc-prompt-face                              ((t (:foreground ,penumbra-green :background ,penumbra-green-light :weight normal))))
   `(erc-timestamp-face                           ((t (:foreground ,penumbra-green :background ,penumbra-green-light))))
   `(erc-direct-msg-face                          ((t (:inherit erc-default))))
   `(erc-notice-face                              ((t (:foreground ,fg-light))))
   `(erc-highlight-face                           ((t (:background ,highlight))))
   `(erc-input-face                               ((t (:foreground ,fg :background ,bg-alt))))
   `(erc-current-nick-face                        ((t (:foreground ,fg :background ,penumbra-cyan-light :weight normal
                                                                   :box (:line-width 1 :style released-button)))))
   `(erc-nick-default-face                        ((t (:weight normal :background ,bg-alt))))
   `(erc-my-nick-face                             ((t (:foreground ,fg :background ,penumbra-cyan-light :weight normal
                                                                   :box (:line-width 1 :style released-button)))))
   `(erc-nick-msg-face                            ((t (:inherit erc-default))))
   `(erc-fool-face                                ((t (:inherit erc-default))))
   `(erc-pal-face                                 ((t (:foreground ,penumbra-purple :weight normal))))
   `(erc-dangerous-host-face                      ((t (:inherit font-lock-warning-face))))
   `(erc-keyword-face                             ((t (:foreground ,penumbra-yellow :weight normal))))

  ;;;;; evil
   `(evil-search-highlight-persist-highlight-face ((t (:inherit lazy-highlight))))

;;;;; flx
   `(flx-highlight-face                           ((t (:foreground ,penumbra-yellow :background ,penumbra-green-light
                                                                   :weight normal :underline t))))

;;;;; company
   `(company-tooltip                              ((t (:background ,penumbra-blue-light))))
   `(company-tooltip-selection                    ((t (:background ,penumbra-cyan-light))))
   `(company-tooltip-common                       ((t (:foreground ,penumbra-blue :bold t))))
   `(company-tooltip-annotation                   ((t (:foreground ,penumbra-yellow :italic t)))) ; parameter hints etc.
   `(company-scrollbar-fg                         ((t (:background ,penumbra-cyan))))
   `(company-scrollbar-bg                         ((t (:background ,penumbra-cyan-light))))
   `(company-preview-common                       ((t (:foreground ,fg :background ,penumbra-cyan-light))))

;;;;; highlight-symbol
   `(highlight-symbol-face                        ((t (:background ,bg-alt))))

;;;;; highlight-numbers
   `(highlight-numbers-number                     ((t (:foreground ,penumbra-blue))))

;;;;; highlight-operators
   `(highlight-operators-face                     ((t (:foreground ,fg))))

;;;;; hl-todo
   `(hl-todo                                      ((t (:inverse-video t))))

;;;;; hl-line-mode
   `(hl-line                                      ((,class (:background ,bg-alt))))

;;;;; hl-sexp
   `(hl-sexp-face                                 ((,class (:background ,bg-alt))))

;;;;; ido-mode
   `(ido-first-match                              ((t (:foreground ,fg :weight normal))))
   `(ido-only-match                               ((t (:foreground ,fg :weight normal))))
   `(ido-subdir                                   ((t (:foreground ,penumbra-blue))))
   `(ido-indicator                                ((t (:foreground ,penumbra-yellow))))

;;;;; ido-vertical
   `(ido-vertical-first-match-face                ((t (:foreground ,fg :background ,penumbra-cyan-light :weight normal))))
   `(ido-vertical-only-match-face                 ((t (:foreground ,penumbra-red :background ,penumbra-red-light :weight normal))))
   `(ido-vertical-match-face                      ((t (:foreground ,fg :background ,penumbra-green-light
                                                                   :weight normal :underline t))))

;;;;; indent-guide
   `(indent-guide-face                            ((t (:foreground ,highlight))))

;;;;; ivy
   `(ivy-current-match                            ((t (:background ,penumbra-blue-light :underline t :extend t))))
   `(ivy-minibuffer-match-face-1                  ((t (:background ,bg-alt))))
   `(ivy-minibuffer-match-face-2                  ((t (:background ,penumbra-cyan-light))))
   `(ivy-minibuffer-match-face-3                  ((t (:background ,penumbra-purple-light))))
   `(ivy-minibuffer-match-face-3                  ((t (:background ,penumbra-blue-light))))

;;;;; js2-mode
   `(js2-warning                                  ((t (:underline ,penumbra-yellow))))
   `(js2-error                                    ((t (:foreground ,penumbra-red :weight normal))))
   `(js2-jsdoc-tag                                ((t (:foreground ,penumbra-purple))))
   `(js2-jsdoc-type                               ((t (:foreground ,penumbra-blue))))
   `(js2-jsdoc-value                              ((t (:foreground ,penumbra-cyan))))
   `(js2-function-param                           ((t (:foreground ,fg))))
   `(js2-external-variable                        ((t (:foreground ,penumbra-cyan))))

;;;;; linum-mode
   `(linum                                        ((t (:foreground ,fg-light))))

;;;;; lsp-mode
   `(lsp-face-highlight-textual                   ((t (:background ,bg-dark))))
   `(lsp-face-highlight-read                      ((t (:background ,penumbra-purple-light))))
   `(lsp-face-highlight-write                     ((t (:background ,penumbra-green-light))))

;;;;; magit
   `(magit-section-heading                        ((t (:foreground ,penumbra-cyan :background ,penumbra-blue-light
                                                                   :weight normal :underline t))))
   `(magit-section-highlight                      ((t (:background ,bg-alt))))
   `(magit-section-heading-selection              ((t (:background ,highlight))))
   `(magit-filename                               ((t (:foreground ,fg))))
   `(magit-hash                                   ((t (:foreground ,penumbra-yellow :weight normal))))
   `(magit-tag                                    ((t (:foreground ,penumbra-purple :weight normal))))
   `(magit-refname                                ((t (:foreground ,penumbra-purple :weight normal))))
   `(magit-head                                   ((t (:foreground ,penumbra-green :weight normal))))
   `(magit-branch-local                           ((t (:foreground ,penumbra-blue :background ,penumbra-blue-light
                                                                   :weight normal))))
   `(magit-branch-remote                          ((t (:foreground ,penumbra-green :background ,penumbra-green-light
                                                                   :weight normal))))
   `(magit-branch-current                         ((t (:foreground ,penumbra-cyan :background ,penumbra-cyan-light
                                                                   :weight normal
                                                                   :box (:line-width 1 :color ,penumbra-cyan)))))
   `(magit-diff-file-heading                      ((t (:foreground ,fg :weight normal))))
   `(magit-diff-file-heading-highlight            ((t (:background ,bg-alt))))
   `(magit-diff-file-heading-selection            ((t (:foreground ,penumbra-red :background ,highlight))))
   `(magit-diff-hunk-heading                      ((t (:foreground ,penumbra-blue :background ,penumbra-blue-light :weight normal :underline t))))
   `(magit-diff-hunk-heading-highlight            ((t (:background ,penumbra-cyan-light))))
   `(magit-diff-added                             ((t (:foreground ,penumbra-green :background ,penumbra-green-light))))
   `(magit-diff-removed                           ((t (:foreground ,penumbra-red :background ,penumbra-red-light))))
   `(magit-diff-context                           ((t (:foreground ,fg-dark :background nil))))
   `(magit-diff-added-highlight                   ((t (:foreground ,penumbra-green :background ,penumbra-green-light))))
   `(magit-diff-removed-highlight                 ((t (:foreground ,penumbra-red :background ,penumbra-red-light))))
   `(magit-diff-context-highlight                 ((t (:foreground ,fg-dark :background ,bg-alt))))
   `(magit-diffstat-added                         ((t (:foreground ,penumbra-green :background ,penumbra-green-light :weight normal))))
   `(magit-diffstat-removed                       ((t (:foreground ,penumbra-red :background ,penumbra-red-light :weight normal))))
   `(magit-log-author                             ((t (:foreground ,penumbra-blue :weight normal))))
   `(magit-log-date                               ((t (:foreground ,penumbra-purple :weight normal))))
   `(magit-log-graph                              ((t (:foreground ,penumbra-red :weight normal))))
   `(magit-blame-heading                          ((t (:foreground ,fg-dark :background ,bg-alt))))

;;;;; paren-face
   `(parenthesis                                  ((t (:foreground "#CCCCB7"))))

;;;;; project-explorer
   `(pe/file-face                                 ((t (:foreground ,fg))))
   `(pe/directory-face                            ((t (:foreground ,penumbra-blue :weight normal))))

;;;;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face              ((t (:foreground ,penumbra-green))))
   `(rainbow-delimiters-depth-2-face              ((t (:foreground ,penumbra-blue))))
   `(rainbow-delimiters-depth-3-face              ((t (:foreground ,penumbra-red))))

;;;;; show-paren
   `(show-paren-mismatch                          ((t (:foreground ,penumbra-yellow :background ,penumbra-red :weight normal))))
   `(show-paren-match                             ((t (:foreground ,fg :background ,penumbra-cyan-light :weight normal))))

;;;;; mode-line/sml-mode-line
   `(mode-line                                    ((,class (:foreground ,fg :background ,penumbra-blue-light :box t))))
   `(mode-line-inactive                           ((t (:foreground ,fg :background ,bg-dark :box t))))
   `(mode-line-buffer-id                          ((t (:foreground ,fg :weight bold)))) ; associated buffer/file name
   `(sml/global                                   ((t (:foreground ,fg))))
   `(sml/modes                                    ((t (:foreground ,penumbra-green :background ,penumbra-green-light))))
   `(sml/filename                                 ((t (:foreground ,penumbra-red))))
   `(sml/folder                                   ((t (:foreground ,fg))))
   `(sml/prefix                                   ((t (:foreground ,fg))))
   `(sml/read-only                                ((t (:foreground ,fg))))
   `(sml/modified                                 ((t (:foreground ,penumbra-red :weight normal))))
   `(sml/outside-modified                         ((t (:background ,penumbra-red :foreground ,penumbra-red-light :weight normal))))
   `(sml/line-number                              ((t (:foreground ,fg :weight normal))))
   `(sml/col-number                               ((t (:foreground ,fg :weight normal))))
   `(sml/vc                                       ((t (:foreground ,fg :weight normal))))
   `(sml/vc-edited                                ((t (:foreground ,penumbra-red :weight normal))))
   `(sml/git                                      ((t (:foreground ,fg :weight normal))))

;;;;; sh
   `(sh-heredoc-face                              ((t (:foreground ,penumbra-purple))))

;;;;; web-mode
   `(web-mode-builtin-face                        ((t (:inherit ,font-lock-builtin-face))))
   `(web-mode-comment-face                        ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-constant-face                       ((t (:inherit ,font-lock-constant-face))))
   `(web-mode-doctype-face                        ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-folded-face                         ((t (:underline t))))
   `(web-mode-function-name-face                  ((t (:foreground ,fg :weight normal))))
   `(web-mode-html-attr-name-face                 ((t (:foreground ,fg))))
   `(web-mode-html-attr-value-face                ((t (:inherit ,font-lock-string-face))))
   `(web-mode-html-tag-face                       ((t (:foreground ,penumbra-blue))))
   `(web-mode-keyword-face                        ((t (:inherit ,font-lock-keyword-face))))
   `(web-mode-preprocessor-face                   ((t (:inherit ,font-lock-preprocessor-face))))
   `(web-mode-string-face                         ((t (:inherit ,font-lock-string-face))))
   `(web-mode-type-face                           ((t (:inherit ,font-lock-type-face))))
   `(web-mode-variable-name-face                  ((t (:inherit ,font-lock-variable-name-face))))
   `(web-mode-server-background-face              ((t (:background ,penumbra-green-light))))
   `(web-mode-server-comment-face                 ((t (:inherit web-mode-comment-face))))
   `(web-mode-server-string-face                  ((t (:foreground ,penumbra-red))))
   `(web-mode-symbol-face                         ((t (:inherit font-lock-constant-face))))
   `(web-mode-warning-face                        ((t (:inherit font-lock-warning-face))))
   `(web-mode-whitespaces-face                    ((t (:background ,penumbra-red-light))))
   `(web-mode-block-face                          ((t (:background ,penumbra-green-light))))
   `(web-mode-current-element-highlight-face      ((t (:foreground ,fg :background ,penumbra-blue-light))))
   `(web-mode-json-key-face                       ((,class (:inherit font-lock-string-face))))
   `(web-mode-json-context-face                   ((,class (:inherit font-lock-string-face :bold t))))

;;;;; which-func-mode
   `(which-func                                   ((t (:foreground ,penumbra-purple :background ,penumbra-purple-light))))

;;;;; yascroll
   `(yascroll:thumb-text-area                     ((t (:background ,highlight))))
   `(yascroll:thumb-fringe                        ((t (:background ,bg :foreground ,bg
                                                                   :box (:line-width 1 :style released-button)))))

;;;;; Org
   `(org-level-1                                  ((t (:background ,penumbra-blue-light :foreground ,penumbra-blue :weight bold :overline t))))
   `(org-level-2                                  ((t (:background ,penumbra-blue-light :foreground ,penumbra-cyan :weight bold :overline t))))
   `(org-level-3                                  ((t (:background ,penumbra-blue-light :foreground ,penumbra-blue :weight bold :overline t))))
   `(org-level-4                                  ((t (:background ,penumbra-blue-light :foreground ,penumbra-cyan))))
   `(org-level-5                                  ((t (:background ,penumbra-blue-light :foreground ,penumbra-blue))))
   `(org-level-6                                  ((t (:background ,penumbra-blue-light :foreground ,penumbra-cyan))))
   `(org-level-7                                  ((t (:background ,penumbra-blue-light :foreground ,penumbra-blue))))
   `(org-level-8                                  ((t (:background ,penumbra-blue-light :foreground ,penumbra-cyan))))
   `(org-document-title                           ((t (:height 1.2 :foreground ,penumbra-blue :weight bold :underline t)))) ; #TITLE
   `(org-meta-line                                ((t (:foreground ,penumbra-green))))
   `(org-document-info                            ((t (:foreground ,penumbra-cyan :weight normal))))
   `(org-document-info-keyword                    ((t (:foreground ,penumbra-cyan))))
   `(org-todo                                     ((t (:foreground ,penumbra-yellow :background ,bg-alt :weight normal :box (:line-width 1 :style released-button)))))
   `(org-done                                     ((t (:foreground ,penumbra-green :background ,penumbra-green-light :weight normal :box (:style released-button)))))
   `(org-date                                     ((t (:foreground ,penumbra-purple))))
   `(org-table                                    ((t (:foreground ,penumbra-purple))))
   `(org-formula                                  ((t (:foreground ,penumbra-blue :background ,bg-alt))))
   `(org-code                                     ((t (:foreground ,penumbra-red :background ,bg-alt))))
   `(org-verbatim                                 ((t (:foreground ,fg :background ,bg-alt :underline t))))
   `(org-special-keyword                          ((t (:foreground ,penumbra-cyan))))
   `(org-agenda-date                              ((t (:foreground ,penumbra-cyan))))
   `(org-agenda-structure                         ((t (:foreground ,penumbra-purple))))
   `(org-block                                    ((t (:foreground ,fg :background ,bg-alt :extend t))))
   `(org-block-background                         ((t (:background ,bg-alt :extend t))))
   `(org-block-begin-line                         ((t (:foreground ,fg-alt :background ,bg-dark :italic t :extend t))))
   `(org-block-end-line                           ((t (:foreground ,fg-alt :background ,bg-dark :italic t :extend t))))

;;;;; origami
   `(origami-fold-replacement-face                ((t (:foreground ,penumbra-red :background ,penumbra-red-light
                                                                   :box (:line-width -1)))))

;;;;; git-gutter
   `(git-gutter:added                             ((t (:background ,penumbra-green-alt :foreground ,penumbra-green-alt :weight normal))))
   `(git-gutter:deleted                           ((t (:background ,penumbra-red :foreground ,penumbra-red :weight normal))))
   `(git-gutter:modified                          ((t (:background ,penumbra-yellow :foreground ,penumbra-yellow :weight normal))))
   `(git-gutter-fr:added                          ((t (:background ,penumbra-green-alt :foreground ,penumbra-green-alt :weight normal))))
   `(git-gutter-fr:deleted                        ((t (:background ,penumbra-red :foreground ,penumbra-red :weight normal))))
   `(git-gutter-fr:modified                       ((t (:background ,penumbra-yellow :foreground ,penumbra-yellow :weight normal))))

;;;;; diff-hl
   `(diff-hl-insert                               ((t (:background ,penumbra-green-alt :foreground ,penumbra-green-alt))))
   `(diff-hl-delete                               ((t (:background ,penumbra-red :foreground ,penumbra-red))))
   `(diff-hl-change                               ((t (:background ,penumbra-yellow :foreground ,penumbra-yellow))))

;;;;; mu4e, mail
   `(mu4e-header-highlight-face                   ((t (:background ,highlight))))
   `(mu4e-unread-face                             ((t (:foreground ,penumbra-blue :weight normal))))
   `(mu4e-flagged-face                            ((t (:foreground ,penumbra-red :background ,penumbra-red-light :weight normal))))
   `(mu4e-compose-separator-face                  ((t (:foreground ,penumbra-green))))
   `(mu4e-header-value-face                       ((t (:foreground ,fg))))
   `(message-header-name                          ((t (:foreground ,penumbra-purple :weight normal))))
   `(message-header-to                            ((t (:foreground ,penumbra-blue))))
   `(message-header-subject                       ((t (:foreground ,penumbra-blue))))
   `(message-header-other                         ((t (:foreground ,penumbra-blue))))
   `(message-cited-text                           ((t (:inherit font-lock-comment-face))))

;;;;; term-mode (vterm too)
   `(term                                         ((,class (:foreground ,fg :background ,bg))))
   `(term-color-black                             ((,class (:foreground ,fg :background ,fg))))
   `(term-color-blue                              ((,class (:foreground ,penumbra-blue :background ,penumbra-blue))))
   `(term-color-red                               ((,class (:foreground ,penumbra-red :background ,penumbra-red))))
   `(term-color-green                             ((,class (:foreground ,penumbra-green :background ,penumbra-green))))
   `(term-color-yellow                            ((,class (:foreground ,penumbra-yellow :background ,penumbra-yellow))))
   `(term-color-magenta                           ((,class (:foreground ,penumbra-purple :background ,penumbra-purple))))
   `(term-color-cyan                              ((,class (:foreground ,penumbra-cyan :background ,penumbra-cyan))))
   `(term-color-white                             ((,class (:foreground ,fg :background ,fg))))

;;;;; fill-column-indicator
   `(fci-rule-color                               ((t (:foreground ,highlight-alt))))
   `(fill-column-indicator                        ((t (:foreground ,highlight-alt))))))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'penumbra)
(provide 'penumbra-theme)

;;; penumbra-theme.el ends here
